var http = require('http');

console.log('Server started...');

http.createServer(function (req, res) {
  name = req.url.split("/")[1];
  res.write(`Hello ${name}`);
  res.end();
}).listen(8080);